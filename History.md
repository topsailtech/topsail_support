2.2.2
=====

  * DatePicker: allow clearing value

2.2.1
=====

  * DatePicker: allow date strings as values instead of only Date(Time)s (allows form filters to use a date picker)

2.2.0
=====
    
  * add NestedAttributesBuilder
  * add DatePickerHelper and DatePickerBuilder
     
2.1.0
=====

  * add #scopes_for_boolean(column_name),
        this generates scopes #{column_name} and not_#{column_name}.
        #{column_name} takes optional arguments (record ID, array of record IDs or an ActiveRecord)
  
2.0.1
=====

  * stable version, running in production systems for years
