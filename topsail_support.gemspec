# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "topsail_support/version"

Gem::Specification.new do |s|
  s.name        = "topsail_support"
  s.version     = Topsail::Support::VERSION
  s.authors     = ["Mark Roghelia"]
  s.email       = ["mroghelia@topsailtech.com"]
  s.summary     = %q{Generic Rails extensions used by other plugins}
  s.files         = `git ls-files`.split("\n")
  s.require_paths = ["lib"]
end
