#
# Make sure you have 
#     <%= javascript_include_tag "jquery.ui.datepicker-#{I18n.locale}.js" %>
# in your application head if you are using non-american locale
# Make sure your rails locale for Date and Time has a format :datepicker
#    that matches your Javascripts I18n format.
#    Example:
#             en.yml:
#                 ....
#                   date:
#                      formats:
#                          datepicker: '%m/%d/%Y'
#                  ....            
#                   time:
#                      formats:
#                          datepicker: '%m/%d/%Y %I:%M%p'
#             Javascript:
#                  $.timepicker.regional['']['timeFormat'] = 'hh:mmtt';
#                  $.timepicker.setDefaults($.timepicker.regional['']);
#                  $.datepicker.regional['']['dateFormat'] = 'mm/dd/yy';
#                  $.datepicker.setDefaults($.datepicker.regional['']);

module DatePickerHelper
  
  def datepicker(object_name, method, options = {})
    date_or_datetimepicker(object_name, method, :datepicker, options)
  end
  
  def datetimepicker(object_name, method, options = {})
    date_or_datetimepicker(object_name, method, :datetimepicker, options)
  end
  
  private
  def date_or_datetimepicker(object_name, method, jqui_plugin_fn, options)
    
    datepicker_display_id = (options[:id] || "#{object_name}_#{method}").gsub(/\]\[|[^-a-zA-Z0-9:.]/, "_").sub(/_$/, "") # display_only field for pretty formatting
    datepicker_id = "#{datepicker_display_id}_altField" # the field that gets submitted

    datepicker_options = options.delete(:datepicker) || {}
    datepicker_options.merge!(altField: '#' + datepicker_id,
                              altFieldTimeOnly: false,
                              altFormat: 'yy-mm-dd',
                              altTimeFormat: 'hh:mm tt')
    
    # build the hidden field that will hold the value to submit
    hidden_field = ActionView::Helpers::InstanceTag.new(object_name, method, self, options.delete(:object))
    
    # the display filed inherits all the additional option stuff, so that CSS can style it as expected
    options.merge!(id: datepicker_display_id, name: 'date_picker_placeholder')
    obj = hidden_field.object
    val = obj && obj.send(method)
    unless val.blank?
      date_val = val.is_a?(String) ? Date.parse(val) : val # if val is a String, it should already be in a  pretty default-parseable format
      internal_val_formatted = date_val.strftime('%Y-%m-%d %I:%M%p')
      options[:value] = I18n.l(date_val, :format => :datepicker)
    end

    hidden_field.to_input_field_tag("hidden", {:id => datepicker_id, :value => internal_val_formatted}) +
      text_field(object_name, method, options) +
      javascript_tag("$(function() {
        $('##{datepicker_display_id}').#{jqui_plugin_fn}(#{datepicker_options.to_json}).
          change(function(){
            if (!$(this).val()) $('##{datepicker_id}').val('');
          });
        })")

  end
  
end

ActionView::Base.send(:include, DatePickerHelper)
