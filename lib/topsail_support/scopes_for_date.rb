class ActiveRecord::Base

  # Creates three named scopes for the given date column:  on_or_after, on_or_before, and during
  def self.scopes_for_date(date_column)
    
    date_column = date_column.to_s
    
    scope "#{date_column}_on_or_after", lambda { |date_from| # Date or Time
      where [ "#{table_name}.#{date_column} >= ?", date_from ]
    }

    scope "#{date_column}_on_or_before", lambda { |date_to| # Date or Time
      if date_to.is_a? Date
        where [ "#{table_name}.#{date_column} < ?", date_to.next ]
      else
        where [ "#{table_name}.#{date_column} <= ?", date_to ] 
      end
    }

    scope "#{date_column}_during", lambda { |*args| # nil, Date, Date Range or Time Range
      if args[0].nil?
        range = Date.today..Date.tomorrow
      elsif args[0].is_a? Date
        range = args[0]..args[0].next
      elsif args[0].is_a? Range and args[0].end.is_a? Date
        range = args[0].begin..args[0].end.next
      else
        range = args[0]
      end
      where "#{table_name}.#{date_column}" => range
    }
    
  end
  
end
