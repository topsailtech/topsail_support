class ActiveRecord::Base

  # creates two scopes (example uses boolean_column 'active'):
  #    active(exeptions) - returns only rows where boolean_column 'active' is true. When exception is provided
  #                        (either as a record ID, or an ActiveRecord, or an array of record IDs),
  #                        also include those rows, even if 'active' is false
  #    not_active - returns all rows for which boolean_column is false
  #
  # suggestion: boolean_column should not be nullable and have some default in the DML
  #
  def self.scopes_for_boolean(boolean_column)

    boolean_column = boolean_column.to_s

    scope boolean_column, lambda { | *exceptions | # nil, record id, array of record ids, an ActiveRecord or array of ActiveRecords
      where(["#{table_name}.#{boolean_column} = ? OR #{table_name}.id IN (?)",
             true,
             exceptions])
    }

    scope "not_#{boolean_column}", lambda {
      where("#{table_name}.#{boolean_column}" => false)
    }

  end

end
