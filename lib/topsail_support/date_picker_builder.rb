module DatePickerBuilder
  def datepicker(method, options = {})
    @template.datepicker(@object_name, method, objectify_options(options))
  end
  
  def datetimepicker(method, options = {})
    @template.datetimepicker(@object_name, method, objectify_options(options))
  end
end

ActionView::Helpers::FormBuilder.send(:include, DatePickerBuilder)

