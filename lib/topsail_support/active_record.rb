class ActiveRecord::Base

  # Before validation, strips leading and trailing whitespace from the specified attributes
  def self.strips_whitespace(*attributes)
    before_validation do | record |
      record.strip_whitespace(attributes)
    end
  end

  # Before validation, converts blank attribute strings to nil
  def self.converts_blank_to_nil(*attributes)
    before_validation do | record |
      record.convert_blank_to_nil(attributes)
    end
  end
  
  # Before validation, strips leading and trailing whitespace from the specificed attributes
  # and converts empty strings to nil
  def self.strips_and_converts_blank_to_nil(*attributes)
    before_validation do | record |
      record.strip_whitespace(attributes)
      record.convert_blank_to_nil(attributes)
    end
  end
 
  # Strips leading and trailing whitespace from the specified attributes
  def strip_whitespace(*attributes)

    attributes = attributes.flatten

    for attribute in attributes
      val = read_attribute(attribute)
      write_attribute(attribute, val.strip) unless val.nil?
    end

  end
  
  # Converts blank attribute strings to nil
  def convert_blank_to_nil(*attributes)
    
    attributes = attributes.flatten
    
    for attribute in attributes
      val = read_attribute(attribute)
      write_attribute(attribute, nil) if !val.nil? and val.blank?
    end
    
  end
  
  # Validate that each of the specified attributes are valid date strings
  def self.validates_date(*attrs)
    
    # Take out the options.  We don't want to pass them through to validates_each because
    # otherwise :allow_nil will not have the effect we want.
    options = attrs.extract_options!.symbolize_keys

    validates_each attrs do | model, attribute, date |
      
      # Get the original, unconverted string
      date_str = model.attributes_before_type_cast[attribute.to_s]

      # A date is treated as invalid when it is one that Rails won't convert for us

      if date.nil? and (options[:allow_nil] != true or (!date_str.nil? and !date_str.blank?) )
        if options.has_key?(:message)
          model.errors.add(attribute.to_s, options[:message])
        else
          model.errors.add(attribute.to_s, "is not a valid date.")
        end
      end
      
    end

  end

end
